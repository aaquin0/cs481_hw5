﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Forms.Maps;

namespace MapsHomework
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Pin> pinNames;
        public MainPage()
        {
            InitializeComponent();

            MyMap.MapType = MapType.Street;
            //Sets the initial map load location
            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.812200, -117.9190), Distance.FromMiles(.25));
            MyMap.MoveToRegion(initialMapLocation);

            PopulatePicker();
        }


        private void PopulatePicker()
        {
            //Instantiate five pins for the map
            pinNames = new ObservableCollection<Pin> // these are all pins of Disneyland
            {
                new Pin
                {
                    Type = PinType.SavedPin, Label="Castle", Address="1313 Disneyland Dr, Anaheim, CA 92802",
                    Position = new Position(33.812839, -117.918991)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Star Wars", Address="1313 Disneyland Dr, Anaheim, CA 92802",
                    Position = new Position(33.811886, -117.918009)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Space Mountain", Address="1313 Disneyland Dr, Anaheim, CA 92802",
                    Position = new Position(33.811114, -117.917497)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Pirates of the Caribbean", Address="1313 Disneyland Dr, Anaheim, CA 92802",
                    Position = new Position(33.811567, -117.920247)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Center of Disneyland", Address="1313 Disneyland Dr, Anaheim, CA 92802",
                    Position = new Position(33.812200, -117.9190)
                }
            };

            PinPicker.Items.Add("Choose a place.");

            foreach (Pin item in pinNames)
            {
                // this loads each of the pins into both the map and picker
                PinPicker.Items.Add(item.Label);
                MyMap.Pins.Add(item);
            }

            PinPicker.SelectedIndex = 0;

        }

        private void SelectedPinChange(object sender, System.EventArgs e)
        {
            var inputSlot = (Picker)sender;
            string pinname = inputSlot.SelectedItem.ToString();
            //When user selects a pin name, find the pin
            foreach (Pin item in pinNames)
            {
                if (item.Label == pinname)
                {
                    //Move the map to center on the pin
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(15));
                }
            }
        }

        private void SwapMapType(object sender, System.EventArgs e)
        {
            //Switch the map to Satellite View
            if(MyMap.MapType == MapType.Street)
            {
                MyMap.MapType = MapType.Satellite;
            }
            else
            {
                MyMap.MapType = MapType.Street;
            }
        }

    }
}
